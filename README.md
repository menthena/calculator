# Calculator

## Getting Started

Install the project dependencies.

```bash
yarn
```

### Development

Start the server in watch mode. It will automatically open the browser and run [http://localhost:3000](http://localhost:3000).

```bash
yarn start
```

### Tests

Run the unit tests.

```bash
yarn test
```

### Build

Production build of the project.

```bash
yarn build
```

---

Credits to https://github.com/facebook/create-react-app
