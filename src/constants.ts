import { TDigit, TOperation } from './types/index.d';

export const DIGITS: TDigit[] = [7, 8, 9, 4, 5, 6, 1, 2, 3, 0];
export const OPERATIONS: TOperation[] = ['/', '+', '-', '*', '='];
