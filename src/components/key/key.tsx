import * as React from 'react';
import { TDigit, TOperation, TAction } from '../../types/index.d';

interface IKeyProps {
  text: TDigit | TOperation | TAction;
  onClick: (value: TDigit | TOperation | TAction) => void;
}

class Key extends React.PureComponent<IKeyProps> {
  handleOnClick = () => {
    const { text, onClick } = this.props;
    onClick(text);
  };
  render() {
    const { text } = this.props;
    return <button onClick={this.handleOnClick}>{text}</button>;
  }
}

export default Key;
