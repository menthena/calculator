import actionTypes from '../actions/actionTypes';
import { IState } from '../types/index.d';
import { reduceInsertDigit, reduceApplyOperation } from './helpers';
import { AnyAction } from 'redux';

export const getDefaultState = (): IState => ({
  operation: '=',
  storedValue: 0,
  value: 0,
});

const reducer = (
  state: IState = getDefaultState(),
  { type, operation, digit }: AnyAction,
): IState => {
  switch (type) {
    case actionTypes.RESET:
      return getDefaultState();
    case actionTypes.INSERT_DIGIT: {
      const { value } = state;
      return {
        ...state,
        value: reduceInsertDigit(value, digit),
      };
    }
    case actionTypes.APPLY_OPERATION:
      return {
        value: 0,
        operation,
        storedValue: reduceApplyOperation(
          state.operation,
          state.value,
          state.storedValue,
        ),
      };
  }
  return state;
};

export default reducer;
